const publicVapidKey = "BAjrK577WZNP7N-1L7MRsoZnHijwulx_IT_538liXNBeQny2aAGn3CyzBNJGf6z-YEaGNCec0kKsNH-0P8CKifQ";

if ('serviceWorker' in navigator) {
    send().catch(err => console.log(err));
}

// Register SW, Register Push, Send the notfication
async function send() {
    console.log('Register Service Worker');
    const register = navigator.serviceWorker.register('/worker.js', {
        scope: '/'
    });
    console.log('Service worker registered');

    console.log('Registering push');
    const subscription = await (await register).pushManager.subscribe({
        userVisibleOnly: true,
        applicationServerKey: urlBase64ToUint8Array(publicVapidKey)
    });
    console.log('Push registered');

    console.log('Sending push');
    await fetch('/subscribe', {
        method: 'POST',
        body: JSON.stringify(subscription),
        headers: {
            'content-type': 'application/json'
        }
    });
    console.log('Push sent');
}

function urlBase64ToUint8Array(base64String) {
    const padding = "=".repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
      .replace(/\-/g, "+")
      .replace(/_/g, "/");
  
    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);
  
    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
  }
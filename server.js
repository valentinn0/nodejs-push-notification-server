const express       = require('express');
const webpush       = require('web-push');
const bodyParser    = require('body-parser');
const path          = require('path');

const app = express();

app.use(express.static(path.join(__dirname, 'client')));
app.use(bodyParser.json());

const publicVapidKey = "BAjrK577WZNP7N-1L7MRsoZnHijwulx_IT_538liXNBeQny2aAGn3CyzBNJGf6z-YEaGNCec0kKsNH-0P8CKifQ";
const privateVapidKey = "tvIspuKcV6BQ19KbPqQ-TWrA2OQTBAxXJLMMzD6m2_0";

webpush.setVapidDetails('mailto: test@test.com', publicVapidKey, privateVapidKey);

// Subscribe route
app.post('/subscribe', (req, res) => {
    // Get push subscription object
    const subscription = req.body;

    // Send 201: resource created
    res.status(201).json({});

    // Create payload
    const payload = JSON.stringify({ title: 'Push test' });

    // Pass object into sendNotification
    webpush.sendNotification(subscription, payload).catch(err => console.log(err));
});

const port = 3000;

app.listen(port, () => console.log(`Server started on port ${port}`));